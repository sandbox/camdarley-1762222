<?php

/**
 * @file
 * Default rule configurations for Commerce Simple Stock.
 */


/**
 * Implements hook_default_rules_configuration().
 */
function commerce_product_availability_default_rules_configuration() {


  $rules = array();


  
	$rules_export = '{ "rules_availability_validate_add_to_cart" : {
	    "LABEL" : "Availability: validate add to cart",
	    "PLUGIN" : "reaction rule",
	    "REQUIRES" : [
	      "commerce_ss",
	      "rules",
	      "commerce_stock",
	      "commerce_product_availability"
	    ],
	    "ON" : [ "commerce_product_availability_add_to_cart_check_product" ],
	    "IF" : [
	      { "commerce_ss_stock_enabled_on_product" : { "commerce_product" : [ "commerce_product" ] } },
	      { "data_is" : {
	          "data" : [ "availability-requested-total" ],
	          "op" : "\u003E",
	          "value" : [ "line-item-availability" ]
	        }
	      }
	    ],
	    "DO" : [
	      { "data_calc" : {
	          "USING" : {
	            "input_1" : [ "line-item-availability" ],
	            "op" : "-",
	            "input_2" : [ "availability-already-booked" ]
	          },
	          "PROVIDE" : { "result" : { "approved_quantity" : "approved quantity" } }
	        }
	      },
	      { "commerce_stock_add_to_cart_set_state" : {
	          "stock_action" : "1",
	          "message" : "The maximum quantity for [commerce-product:title] that can be ordered is \u003C?php echo $line_item_availability; ?\u003E.",
	          "approved_quantity" : [ "approved-quantity" ]
	        }
	      }
	    ]
	  }
	}';
  $rules['rules_availability_validate_add_to_cart'] = rules_import($rules_export);
  
  
  $rules_export = '{ "rules_availability_validate_checkout" : {
    "LABEL" : "Availability: validate checkout",
    "PLUGIN" : "reaction rule",
    "REQUIRES" : [
      "commerce_ss",
      "rules",
      "php",
      "commerce_stock",
      "commerce_product_availability"
    ],
    "ON" : [ "commerce_product_availability_check_product_checkout" ],
    "IF" : [
      { "commerce_ss_stock_enabled_on_product" : { "commerce_product" : [ "commerce_product" ] } },
      { "data_is" : {
          "data" : [ "availability-already-booked" ],
          "op" : "\u003E",
          "value" : [ "line-item-availability" ]
        }
      }
    ],
    "DO" : [
      { "commerce_stock_checkout_state" : {
          "stock_action" : "1",
          "message" : "The maximum quantity for [commerce-product:title] that can be ordered on [commerce-line-item:commerce-booking-date] is \u003C?php echo $line_item_availability; ?\u003E.",
          "approved_quantity" : [ "line-item-availability" ]
        }
      }
    ]
  }
}';
$rules['rules_availability_validate_checkout'] = rules_import($rules_export);

  return $rules;
}