<?php

/**
 * @file
 * Rules integration for Commerce Stock.
 */
 
/**
 * Implements hook_rules_event_info().
 */
function commerce_product_availability_rules_event_info() {
  $events = array();

  $events['commerce_product_availability_add_to_cart_check_product'] = array(
    'label' => t('Check if a product is available on requested date when adding to cart'),
    'group' => t('Commerce Stock'),
    'variables' => commerce_product_availability_rules_event_variables(),
    'access callback' => 'commerce_stock_rules_access',
  );

  $events['commerce_product_availability_check_product_checkout'] = array(
    'label' => t('Check if a product is available on requested date before continuing to checkout'),
    'group' => t('Commerce Stock'),
    'variables' => commerce_product_availability_rules_cart_event_variables(),
    'access callback' => 'commerce_stock_rules_access',
  );

  return $events;
}

/**
 * Returns a variables array for stock check event.
 */
function commerce_product_availability_rules_event_variables() {
  $variables = array(
    'commerce_product' => array(
      'label' => t('Product'),
      'type' => 'commerce_product',
    ),
    'availability_requested_quantity' => array(
      'label' => t('Requested Quantity'),
      'type' => 'decimal',
    ),
    'line_item_availability' => array(
      'label' => t('Quantity available on this date'),
      'type' => 'decimal',
    ),
    'availability_already_booked' => array(
      'label' => t('Quantity already booked (in the cart)'),
      'type' => 'decimal',
    ),
    'availability_requested_total' => array(
      'label' => t('Quantity requested + already booked'),
      'type' => 'decimal',
    ),
  );
  return $variables;
}

/**
 * Returns a variables array for stock enable cart check event.
 */
function commerce_product_availability_rules_cart_event_variables() {
  $variables = array(
    'commerce_line_item' => array(
      'label' => t('Line item'),
      'type' => 'commerce_line_item',
    ),
    'commerce_product' => array(
      'label' => t('Product'),
      'type' => 'commerce_product',
    ),
    'availability_already_booked' => array(
      'label' => t('Quantity already booked (in the cart)'),
      'type' => 'decimal',
    ),
    'line_item_availability' => array(
      'label' => t('Availability of line-item'),
      'type' => 'decimal',
    ),
  );
  return $variables;
}
