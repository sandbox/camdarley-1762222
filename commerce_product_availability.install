<?php

/**
 * @file
 * Install for Commerce Product Availability module - sets up the main Commerce Product Availability
 * table
 */


/**
 * Implements hook_schema().
 */
function commerce_product_availability_schema() {
  $schema = array();

  $schema['commerce_product_availability'] = array(
    'description' => 'The main table holding availability information',
    'fields' => array(
      'product_id' => array(
        'description' => 'Identifier for a product.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'year' => array(
        'description' => 'The calendar year for which this availability row is relevant',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0',
      ),
      'month' => array(
        'description' => 'The month for which this availability row is relevant',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0',
      ),
    ),
    'indexes' => array(
      'product_id' => array('product_id'),
      'year' => array('year'),
      'month' => array('month'),
    ),
    'unique keys' =>  array(
      'month_key' => array('product_id', 'year', 'month'),
    ),
    'foreign keys' => array(
      'product_id' => array(
        'table' => 'commerce_product',
        'columns' => array('product_id' => 'product_id'),
      ),
    ),
  );

  $schema['commerce_product_availability_booked'] = array(
    'description' => 'The table holding left stock availability information',
    'fields' => array(
      'product_id' => array(
        'description' => 'Identifier for a product.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'year' => array(
        'description' => 'The calendar year for which this availability row is relevant',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0',
      ),
      'month' => array(
        'description' => 'The month for which this availability row is relevant',
        'type' => 'int',
        'not null' => TRUE,
        'default' => '0',
      ),
    ),
    'indexes' => array(
      'product_id' => array('product_id'),
      'year' => array('year'),
      'month' => array('month'),
    ),
    'unique keys' =>  array(
      'month_key' => array('product_id', 'year', 'month'),
    ),
    'foreign keys' => array(
      'product_id' => array(
        'table' => 'commerce_product',
        'columns' => array('product_id' => 'product_id'),
      ),
    ),
  );
  
  for($i = 1; $i <= 31; $i++) {
    $field = array(
      'description' => 'Month Day ' . $i,
      'type' => 'int',
      'not null' => TRUE,
      'default' => '0',
    );
    $schema['commerce_product_availability_booked']['fields']['d'.$i] = $field;
    $schema['commerce_product_availability']['fields']['d'.$i] = $field;
  }

  
  return $schema;
}

function commerce_product_availability_install() {
  // Create the 'taxonomy_forums' field if it doesn't already exist.
  $field_name = 'commerce_booking_date';
  $field = field_info_field($field_name);
	if (empty($field)) {
	  $field = array(
	    'field_name' => $field_name, 
	    'type' => 'datetime',
	    'entity_types' => array('commerce_line_item'),
	    'active' => TRUE, // default
	    'settings' => array(
	      'granularity' => array(
          'year' => 'year',
          'month' => 'month',
          'day' => 'day',
        ),
        'tz_handling' => 'none',
        'timezone_db' => '',
        'repeat' => 0,
        'todate' => '',
      ),
	  );
	  field_create_field($field);
	}
	
	// Attach the field to the node type
	$instance = array(
	  'field_name' => $field['field_name'],
	  'entity_type' => 'commerce_line_item',
	  'bundle' => 'bookable_product',
	  'required' => TRUE,
	  'label' => t('Booking Date'),
	  'description' => '', // default
	  'deleted' => FALSE, // default
	  'widget' => array(
	    'weight' => 1,
	    'type' => 'date_select',
	    'active' => TRUE, // default
	    'settings' => array(
	      'input_format' => 'm/d/Y',
	      'year_range' => '-3:+3', // reversed year range '+3:-3' is not supported
	      'increment' => 1, // default, if it set to 2, then the minutes will increment by 2: 0, 2, 4, 6
	      'label_position' => 'within',
	      'text_parts' => array(), // If you would like to use both text field and select options
	      'repeat_collapsed' => FALSE, // default
	    ),
	  ),
	  'settings' => array(
	    'default_value' => 'now',
	    'default_value_code' => '', // default
	    'default_value2' => 'blank', // default
	    'default_value_code2' => '', // default
	    'default_format' => 'standard', // Custom date type
	  ),
	  'display' => array(
	    'default' => array(
	      'label' => 'above',
	      'type' => 'date_standard',
	      'weight' => 1,
	      'settings' => array(
	        'format_type' => 'standard',
	        'fromto' => 'both',
	        'multiple_number' => '',
	        'multiple_from' => '',
	        'multiple_to' => '',
	        'show_repeat_rule' => 'show',
	      ),
	    ),
	  ),
	  'commerce_cart_settings' => TRUE,
	);
	field_create_instance($instance);
	
}

function commerce_product_availability_uninstall() {
  $field_name = 'commerce_booking_date';
  $field = field_info_field($field_name);
  if (empty($field)) field_delete_field($field['commerce_booking_date']);
}