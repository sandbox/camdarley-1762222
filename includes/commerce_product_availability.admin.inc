<?php

/**
 * Callback for admin/commerce/products/%unit/availability - builds availability
 * page by adding calendar and pulling events from availability table.
 * 
 */
function commerce_product_availability_page($product, $year='', $month='') {
  
  // Basic check to avoid any uggliness
  $year = check_plain($year);
  $month = check_plain($month);
  
  // If year is not set then give it the current date
  $year = ($year == '') ? date('Y', time()) : $year;
  $month = ($month == '') ? date('n', time()) : $month;

  // Add all the stuff we will need to show the FullCalendar  
  drupal_add_library('rooms_availability', 'rooms_fullcalendar');
  drupal_add_js(drupal_get_path('module', 'commerce_product_availability') . '/js/commerce_product_availability.js');
  drupal_add_css(drupal_get_path('module', 'rooms_availability') . '/css/fullcalendar.theme.css');
  drupal_add_css(drupal_get_path('module', 'commerce_product_availability') . '/css/commerce_product_availability.css');
  
  // Inject settings in javascript that we will use
  drupal_add_js(array('productsAvailability' => array('productID' => $product->product_id)), 'setting');
  drupal_add_js(array('productsAvailability' => array('currentMonth' => $month)), 'setting');
  drupal_add_js(array('productsAvailability' => array('currentYear' => $year)), 'setting');
  drupal_add_js(array('productsAvailability' => array('event_style' => ROOMS_AVAILABILITY_ADMIN_STYLE)), 'setting');
  

  // Calculate forward and back dates for our 3-month view calendar and create links
  $date1 = new DateTime("$year-$month-1");
  $date2 = new DateTime("$year-$month-1");
  $date_current = new DateTime("now");


  $forward = $date1->add(new DateInterval('P3M'));
  $forward_path = 'admin/commerce/products/' . $product->product_id . '/availability/' . $forward->format('Y') . '/' . $forward->format('n') . '/1';
  $forward_link = l(t('Forward'), $forward_path);

  $backward = $date2->sub(new DateInterval('P3M'));
  $backward_path = 'admin/commerce/products/' . $product->product_id . '/availability/' . $backward->format('Y') . '/' . $backward->format('n') . '/1';
  $backward_link = l(t('Back'), $backward_path);
  
  $current_path = 'admin/commerce/products/' . $product->product_id . '/availability/' . $date_current->format('Y') . '/' . $date_current->format('n') . '/1';
  $current_link = l(t('Current'), $current_path);
  
  // The content to theme
  $content = array();

  $content['title'] = array(
    '#prefix' => '<div class="availability-title">',
    '#markup' => '<h2>' . t('@name Availability View', array('@name' => $product->sku)) . '</h2>',
    '#suffix' => '</div>'
  );
  $content['sku'] = $product->sku;
  $content['type'] = $product->type;
  $content['update_form'] = drupal_get_form('commerce_product_availability_update_calendar_form', $product->product_id, $year, $month);
  $content['start_year'] = $year;
  $content['start_month'] = $month;
  $content['forward_link'] = $forward_link;
  $content['backward_link'] = $backward_link;
  $content['current_link'] = $current_link;

  $content['update_form_into'] = array(
    '#prefix' => '<div class="availability-update">',
    '#markup' => '<h2>' . t('Update Product Availability') . '</h2>',
    '#suffix' => '</div>'
  );
  
  // Send everything for theming
  $output = theme('rooms_availability', $content);

  return $output;
}

/** 
 * Creates the necessary json for the date range provided - needs at least start year and month at which point it will
 * return the entire month.
 */
function commerce_product_availability_event($product, $event_style = ROOMS_AVAILABILITY_ADMIN_STYLE, $start_year = '', $start_month = '', $start_day = '') {

  if (!$event_style) $event_style = ROOMS_AVAILABILITY_ADMIN_STYLE;
  
  $start_year = (int)$start_year;
  $start_month = (int)$start_month;
  $start_day = (int)$start_day;
  
  $event_style = (int)$event_style;
  
  $eom = rooms_end_of_month_dates($start_year);
    
  if (($start_year == 0) || ($start_month == 0)) {
    echo drupal_json_encode('missing basic info');
    return;
  }
  elseif ($start_day == 0) {
    $start_date = new DateTime("$start_year-$start_month-1");
    $end_day = $eom[$start_month];
    $end_date = new DateTime("$start_year-$start_month-$end_day");
  }
  elseif ($start_day != 0) {
    $start_date = new DateTime("$start_year-$start_month-$start_day");
    $end_date = new DateTime("$start_year-$start_month-15");
    $end_date->add(new DateInterval('P1M'));
    $end_year = $end_date->format('Y');
    $end_month = $end_date->format('n');
    $end_day = $eom[$end_date->format('n')];
    $end_date = new DateTime("$end_year-$end_month-$end_day");
  }
  else{
    $start_date = new DateTime("$start_year-$start_month-$start_day");
    $end_date = new DateTime("$end_year-$end_month-$end_day");
  }
  
  
  $rc = new ProductCalendar($product->product_id, $event_style);
  $events_stock = $rc->getEvents($start_date, $end_date);
  
  $rc_booked = new BookingCalendar($product->product_id);
  $events_booked = $rc_booked->getEvents($start_date, $end_date, true);

  foreach($events_booked as &$event_booked) {
    $event_booked->calculateAvailabiliy($events_stock);
  }
  

  // if we are in admin management
  if ($event_style == ROOMS_AVAILABILITY_ADMIN_STYLE) $events = array_merge($events_stock, $events_booked);
  else $events = $events_stock;
  
  $json_events  = array();
  
  foreach ($events as $key => $event) {
    $json_events[] = $event->formatJson($event_style);
  }
 
  echo drupal_json_encode($json_events);
  
}

/**
 * A basic form that allows us to update the state of the calendar
 */
function commerce_product_availability_update_calendar_form($form, &$form_state, $product_id, $year, $month) {
  
  $form['commerce_product_update_availability'] = array(
    '#type' => 'fieldset',
    '#title' => 'Update Availability',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['commerce_product_update_availability']['product_id'] = array(
    '#type' => 'hidden',
    '#value' => $product_id,
  );
  
  $form['commerce_product_update_availability']['products_date_range'] = rooms_date_range_fields();
  // Unset a js setting
  drupal_add_js(array('rooms' => array('roomsBookingStartDay' => 0)), 'setting');
  
  $product = commerce_product_load($product_id);
  $default_value = isset($product->commerce_stock[LANGUAGE_NONE][0]['value']) ? isset($product->commerce_stock[LANGUAGE_NONE][0]['value']) : 0;
  
  $form['commerce_product_update_availability']['product_stock'] = array(
    '#type' => 'textfield',
    '#title' => t('Product Stock'),
    '#default_value' => $default_value,
    '#description' => t('Choose how many stock to put the product in for the dates chosen above'),
  );
  
  $form['commerce_product_update_availability']['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );
  
  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['commerce_product_update_availability']['#submit'])) {
    $submit += $form['commerce_product_update_availability']['#submit'];
  }
  
  $form['commerce_product_update_availability']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Room Availability'),
    '#submit' => $submit + array('commerce_product_availability_update_availability_calendar_form_submit'),
  );
  
  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'commerce_product_availability_update_availability_calendar_form_validate';
  
  
  return $form;
}

function commerce_product_availability_update_availability_calendar_form_validate(&$form, &$form_state) {
  // Check date validity
  if ($form_state['values']['rooms_start_date']['date'] == '') {
    return;
  }
  if ($form_state['values']['rooms_end_date']['date'] == '') {
    return;
  }
  if ($form_state['values']['product_stock']['value'] == '') {
    return;
  }
  $date1 = new DateTime($form_state['values']['rooms_start_date']);
  $date2 = new DateTime($form_state['values']['rooms_end_date']);
  $diff = $date1->diff($date2);
  // If date1 > date2
  if ($diff->invert) {
    form_set_error('date_range', t('End date must be after start date'));
  }
  if (is_int($form_state['values']['product_stock'])) {
    form_set_error('stock', t('Stock field must be an integer'));
  }
}


/**
 *@todo Need to figure out what to do when we cancel an existing booking
 */

function commerce_product_availability_update_availability_calendar_form_submit(&$form, &$form_state) {
 
  $form_state['redirect'] = array('admin/commerce/products/' . $form_state['values']['product_id'] . '/availability');
  
  $start_date = new DateTime($form_state['values']['rooms_start_date']);
  $end_date = new DateTime($form_state['values']['rooms_end_date']);
  $event_id = $form_state['values']['product_stock'];
  
  $product_id = $form_state['values']['product_id'];
  
  // Create a new Booking Event
  $be = new ProductBookingEvent($product_id, $event_id, $start_date, $end_date);

  $events = array($be);
  $rc = new ProductCalendar($product_id);
  
  $response = $rc->updateCalendar($events);
  
  if ($response[$event_id] == ROOMS_UPDATED) {
    drupal_set_message(t('Calendar Updated'));
  }
}

/**
 * The EventManager page shows when clicking on an event in the availability calendar - will allow a user to manipulate
 * that event.
 */
function commerce_product_availability_event_manager_page($product, $event_id = NULL, $start_date = 0, $end_date = 0, $booked = false) {
  
  if ($booked == 'true') {
    $date = new DateTime();
    $date->setTimezone(new DateTimeZone('UTC'))->setTimestamp($start_date);
    $date_format = ('Y-m-d');
    $content['event_details'] = array(
      '#markup' => views_embed_view('line_items_on_date', 'page', $date->format($date_format)),
    );
  }
  
  else {
	  // If any info missing we cannot load the event
	  if ($event_id == NULL || $start_date == 0 || $end_date == 0) {
	    return 'Unable to load event';
	  }
	  
	  // Basic check to avoid damage from dirty input
	  $event_id = check_plain($event_id);
	  $start_date = check_plain($start_date);
	  $end_date = check_plain($end_date);
	  
	  $sd = new DateTime();
	  $sd->setTimezone(new DateTimeZone('UTC'))->setTimestamp($start_date);
	  
	  $ed = new DateTime();
	  $ed->setTimezone(new DateTimeZone('UTC'))->setTimestamp($end_date);
	
	  $content['event_title'] = array(
	    '#prefix' => '<h2>',
	    '#markup' => t('@status @product are available', array('@product' => $product->title, '@status' => $event_id)),
	    '#suffix' => '</h2>',
	  );
	  $date_format = variable_get('rooms_date_format', 'd-m-Y');
	  $content['event_details'] = array(
	    '#prefix' => '<div class="event-details">',
	    '#markup' => t('From @startdate to @enddate', array('@startdate' => $sd->format($date_format), '@enddate' => $ed->format($date_format))),
	    '#suffix' => '</div>'
	  );
	  
	  $content['event_manager_form'] = drupal_get_form('commerce_product_availability_event_manager_form', $product->product_id, $event_id, $start_date, $end_date);
  
  }
  
  $output = theme('rooms_event', $content);
  
  return $output;
}

/**
 * The Event Manager Form - will eventually change based on the type of event we are manipulating
 */
function commerce_product_availability_event_manager_form($form, $form_state, $product_id, $event_id, $start_date, $end_date) {
  $form = array();
  
  $form['form_wrapper_top'] = array(
    '#type' => 'item',
    '#markup' => '',
    '#prefix' => '<div id="replace_textfield_div">',
  );

  
  $form['product_id'] = array(
    '#type' => 'hidden',
    '#value' => $product_id,
  );

  $form['event_id'] = array(
    '#type' => 'hidden',
    '#value' => $event_id,
  );
  
  $form['start_date'] = array(
    '#type' => 'hidden',
    '#value' => $start_date,
  );
  
  $form['end_date'] = array(
    '#type' => 'hidden',
    '#value' => $end_date,
  );
  
  $form['change_event_status'] = array(
    '#type' => 'textfield',
    '#title' => t('Product Stock'),
    '#default_value' => $event_id,
    '#description' => t('Choose how many stock to put the product in for the dates chosen above'),
  );
  
  $form['actions']['update'] = array(
    '#type' => 'button',
    '#value' => t('Update Product Availability'),
    '#ajax' => array(
        'callback' => 'commerce_product_availability_event_manager_form_submit',
        'wrapper' => 'replace_textfield_div',
    ),
  );

  // This entire form element will be replaced whenever 'changethis' is updated.
  $form['form_wrapper_bottom'] = array(
    '#type' => 'item',
    '#markup' => '',
    '#suffix' => '</div>',
  );

  return $form;

}

/**
 * The callback for the change_event_status widget of the event manager form
 */
function commerce_product_availability_event_manager_form_submit($form, $form_state) {
  
  $product_id = $form_state['values']['product_id'];
  $event_id = $form_state['values']['event_id'];
  $start_time = $form_state['values']['start_date'];
  $end_time = $form_state['values']['end_date'];
  
  $new_event_id = $form_state['values']['change_event_status'];
  
  // If we have a new event id go ahead and update event
  if (($event_id != $new_event_id) && $new_event_id != -1) {
    
    $start_date = new DateTime();
    $start_date->setTimestamp($start_time);
    
    $end_date = new DateTime();
    $end_date->setTimestamp($end_time);  
    
    $event = new ProductBookingEvent($product_id, $new_event_id, $start_date, $end_date);
    $uc = new ProductCalendar($product_id);
    $responses = $uc->updateCalendar(array($event));
    
    $form['form_wrapper_bottom']['#markup'] = 'Original Event id is:' . $event_id . ' New Event id is:' . $new_event_id;
    
  }
  
  return $form;
}