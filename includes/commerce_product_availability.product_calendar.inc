<?php

/**
 * Handles quering and updating the availability information
 * relative to a single bookable product.
 */
class ProductCalendar {

  // The bookable product the Calendar is relevant to  
  protected $product_id;
  
  // The default stock for the product if it has no specific booking
  protected $default_stock;
  
  // The table for sql queries
  protected $table;
  
  protected $public_style;
  
  
  public function __construct($product_id, $public_style) {
    $this->public_style = $public_style;
    $this->product_id = $product_id;
    // Load the product entity
    $product = commerce_product_load($product_id);
    if (isset($product->commerce_stock[LANGUAGE_NONE][0]['value'])) $this->default_stock = intval($product->commerce_stock[LANGUAGE_NONE][0]['value']);
    else $this->default_stock = 0;
    $this->table = 'commerce_product_availability';
    
  }
  
  
  /**
   * Given an array of events removes events from the calendar setting the value to the default 
   *
   * @param $events
   *    The events to remove from the database - an array of Booking Events
   *
   * @return
   *   The array of ids of events that were found and removed
   */
  public function removeEvents($events) {
    
    $events_to_delete = array();
    foreach ($events as $event) {
      // Break any locks
      $event->unlock();
      // Set the events to the default stock
      $event->id = $this->default_stock;
      
      $events_to_delete[] = $event;
    }
    $this->updateCalendar($events_to_delete);
  }
  
  
  /**
   * Given a quantity we compare against the stock the product actually has.
   *
   * If the product is out of stock - hence no availability.
   *
   * @param $date
   * The date for the search
   *
   * @param $qte
   * The quantity we are interested in
   *
   * @return
   * Returns true if the date range provided does not have stocks other than the ones we are
   * interested in
   */
  public function stockAvailability(DateTime $date, $qte = 1) {
    
    $valid = TRUE;
    
    // Get all stock in the date range
    $stock = $this->getStock($start_date, $end_date);
    // Look at the difference between existing stocks and stocks to check against -
    $valid = ($stock > $qte) ? FALSE : TRUE;
    return $valid;
  }
  
  
  /**
   * Given a date range returns an array of BookingEvents. The heavy lifting really takes place in
   * the getRawDayData function - here we are simply acting as a factory for event objects
   *
   * @param $start_date
   * The starting date
   *
   * @param $end_date
   * The end date of our range
   *
   * @returns
   * An array of BookingEvent objects
   */
  public function getEvents(DateTime $start_date, DateTime $end_date, $offset = false) {
    // Format dates for use later on
    $start_day = $start_date->format('j');
    $end_day = $end_date->format('j');
     
    // Get the raw day results
    $results = $this->getRawDayData($start_date, $end_date, $offset);
    $events = array();
    foreach ($results[$this->product_id] as $year => $months) {
      $eod = rooms_end_of_month_dates($year);
      foreach ($months as $mid => $month) {
        // The event array gives us the start days for each event within a month
        $start_days = array_keys($month['stocks']);
        foreach ($month['stocks'] as $stock) {
          // Create a booking event 
          $start = $stock['start_day'];
          $end= $stock['end_day'];
          $sd = new DateTime("$year-$mid-$start");
          $ed = new DateTime("$year-$mid-$end");
                    
          $event = new ProductBookingEvent($this->product_id,
                                    $stock['stock'],
                                    $sd,
                                    $ed);
          $events[] = $event;
        }    
      }
    }
    return $events;
  }
  
  
  /**
   * Given a date range returns all stocks in that range - useful when we are not interested
   * in starting and ending dates but simply in stocks
   *
   * @param $start_date
   * The start day of the range
   * 
   * @param $end_date
   * The end date of our range
   *
   * @returns
   * An array of stocks within that range
   */
  public function getStocks(DateTime $start_date, DateTime $end_date) {    
    
    $stocks = array();
    // Get the raw day results
    $results = $this->getRawDayData($start_date, $end_date);
    foreach ($results[$this->product_id] as $year => $months) {
      foreach ($months as $mid => $month) {
        foreach ($month['stocks'] as $stock) {
          if ($stock['stock'] < 0) {
            $stocks[] = -1;
          }
          else {
            $stocks[] = $stock['stock'];
          }
        } 
      }
    }
    $stocks = array_unique($stocks);
    return $stocks;
  }
  
  
  /**
   * Given a date range it returns all data within that range including the start and
   * end dates of stocks. The MySQL queries are kept simple and then the data is cleared up
   * 
   * @param $start_date
   * The starting date
   *
   * @param $end_date
   * The end date of our range
   *
   * @return
   * An array of the structure data[productid][year][month][days][d1]..[d31]
   * as wekk as data[productid][year][month][unique_stocks]
   */
  public function getRawDayData(DateTime $start_date, DateTime $end_date, $offset = 0) {
    
    // Create a dummy BookingEvent to represent the range we are searching over
    // This gives us access to handy functions that BookingEvents have
    $s = new ProductBookingEvent($this->product_id, 0, $start_date, $end_date);
    
    // load bookings for these day
    $rc_booked = new BookingCalendar($this->product_id);
    $booked = $rc_booked->getRawDayData($start_date, $end_date);
    
    $results = array();
    
    // Start by doing a query to the db to get any info stored there
    
    // If search across the same year do a single query
    if ($s->sameYear()) {
      $query = db_select($this->table, 'a');
      $query->fields('a');
      $query->condition('a.product_id', $this->product_id);
      $query->condition('a.year', $s->startYear());
      $query->condition('a.month', $s->startMonth(), '>=');
      $query->condition('a.month', $s->endMonth(), '<=');
      $months = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
      if (count($months) > 0) {
        foreach ($months as $month) {
          $m = $month['month'];
          $y = $month['year'];
          $id = $month['product_id'];
          // Remove the three first rows and just keep the days
          unset($month['month']);
          unset($month['year']);
          unset($month['product_id']);
          $results[$id][$y][$m]['days'] = $month;
        }
      }
    }
    // For multiple years do a query for each year
    else {
      for ($j = $s->startYear(); $j <= $s->endYear(); $j++) {
        $query = db_select($this->table, 'a');
        $query->fields('a');
        $query->condition('a.product_id', $this->product_id);
        $query->condition('a.year', $j);
        if ($j == $s->startYear()) {
          $query->condition('a.month', $s->startMonth(), '>=');
        }
        elseif ($j == $s->endYear()) {
          $query->condition('a.month', $s->endMonth(), '<=');
        }
        $months = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
        if (count($months) > 0) {
          foreach ($months as $month) {
            $m = $month['month'];
            $y = $month['year'];
            $id = $month['product_id'];
            unset($month['month']);
            unset($month['year']);
            unset($month['product_id']);
            $results[$id][$y][$m]['days'] = $month;
          }
        }
      }
    }
    
    if ($this->public_style == ROOMS_AVAILABILITY_GENERIC_STYLE) {
      // We adjust the availability value 
      foreach($results[$this->product_id] as $year => $months) {
        foreach($months as $mid => $days) {
          foreach($days['days'] as $day => $quantity) {
            $day_number = substr($day, 1);
            $date = new DateTime($year . '-' . $mid . '-' . $day_number);
            if (isset($booked[$date->format('Y-m-d H:i:s')])) {
              $left_quantity = $quantity - $booked[$date->format('Y-m-d H:i:s')];
              if ($left_quantity <= 0) {
                $results[$this->product_id][$year][$mid]['days']['d' . $day_number] = 0;
              }
              else $results[$this->product_id][$year][$mid]['days']['d' . $day_number] = 1;
            }
            elseif ($quantity > 0) $results[$this->product_id][$year][$mid]['days']['d' . $day_number] = 1;
            else $results[$this->product_id][$year][$mid]['days']['d' . $day_number] = 0;
          }
        }
      }
    }
    
    
    
    // With the results from the db in place fill in any missing months
    // with the default stock for the product
    for ($j = $s->startYear(); $j <= $s->endYear(); $j++) {
      $eod = rooms_end_of_month_dates($j);

      // We start by setting the expected start and end months for each year
      if ($s->sameYear()) {
        $expected_months = $s->endMonth() - $s->startMonth() +1;
        $sm = $s->startMonth();
        $em = $s->endMonth();
      }
      elseif ($j == $s->endYear()) {
        $expected_months = $s->endMonth();
        $sm = 1;
        $em = $s->endMonth();
      }
      elseif ($j == $s->startYear()) {
        $expected_months = 12 - $s->startMonth() +1;
        $em = 12;
        $sm = $s->startMonth();
      }
      else {
        $expected_months = 12;
        $sm = 1;
        $em = 12;
      }
      
      // We then check to see if the months we have already fit our expectations
      $actual_months =  isset($result[$this->product_id][$j]) ? count($results[$id][$j]) : 0;
      if ($expected_months>$actual_months) {
        // We have missing months so lets go fill them
        for ($i = $sm; $i <= $em; $i++) {
          if (!isset($results[$this->product_id][$j][$i])) {
            $last_day = $eod[$i];
            $month = $this->prepareFullMonthArray(new ProductBookingEvent($this->product_id,
                                                                   $this->default_stock,
                                                                   new DateTime("$j-$i-1"), 
                                                                   new DateTime("$j-$i-$last_day")
                                                                   ));
            // Add the month in its rightful position
            $results[$this->product_id][$j][$i]['days'] = $month;
            // And sort months
            ksort($results[$this->product_id][$j]);
          }
        }
      }        
    }

    // With all the months in place we now need to clean results to set the right start and end date
    // for each month - this will save code downstream from having to worry about it
    foreach ($results[$this->product_id] as $year => $months) {
      foreach ($months as $mid => $days) {
        // Get the end of month values again to make sure we have the right year because it might change
        // for queries spanning years
        $eod = rooms_end_of_month_dates($year);
        // There is undoubtetly a smarter way to do the clean up below - but will live with this for now
        if (count($days['days']) != $eod[$mid]) {
          switch ($eod[$mid]) {
            case 30:
              unset($results[$this->product_id][$year][$mid]['days']['d31']);
              break;
            case 29:
              unset($results[$this->product_id][$year][$mid]['days']['d31']);
              unset($results[$this->product_id][$year][$mid]['days']['d30']);
              break;
            case 28:
              unset($results[$this->product_id][$year][$mid]['days']['d31']);
              unset($results[$this->product_id][$year][$mid]['days']['d30']);
              unset($results[$this->product_id][$year][$mid]['days']['d29']);
              break;
          }
        }
        if (($year == $s->startYear()) && ($mid == $s->startMonth())) {
          // We know we have the entire months over the range so we just unset all the dates
          // from the start of the month to the actual start day
          for ($i = 1; $i < $s->startDay(); $i++) {
            unset($results[$this->product_id][$year][$mid]['days']['d' . $i]);
          }
        }
        if (($year == $s->endYear()) && ($mid == $s->endMonth())) {
          // and from the end of the month back to the actual end day
          for ($i = $s->endDay()+1; $i <= $eod[$mid]; $i++) {
            unset($results[$this->product_id][$year][$mid]['days']['d' . $i]);
          }
        }

      }
    }
    
    // With the results in place we do a stocks array with the start and
    // end dates of each event
    foreach ($results[$this->product_id] as $year => $months) {
      foreach ($months as $mid => $days) {
        // The number of days in the month we are interested in eventing
        $j = count($days);
        // The start date (in case we are not starting from the first day of the month)
        $i = substr(key($days['days']), 1);
        $start_day = $i;
        $diff = 0;
        $end_day = NULL;
        $unique_stocks = array();
        $old_stock = $days['days']['d' . $i];
        $stock = $days['days']['d' . $i];
        while ($j <= count($days['days'])) {          
          $stock = $days['days']['d' . $i];
          if (($stock != $old_stock) || (($offset) && ($diff >= $offset))) {
            $unique_stocks[] = array('stock' => $old_stock,
                                               'start_day' => $start_day,
                                               'end_day' => $i-1);
            $end_day = $i - 1;
            $start_day = $i;
            $old_stock = $stock;
            $diff = 0;
          }
          $i++;
          $j++;
          $diff++;
        }
        // Get the last event in
        $unique_stocks[] = array('stock' => $stock,
                                   'start_day' => isset($end_day) ? $end_day+1 : $start_day,
                                   'end_day' => $i-1);
        $results[$this->product_id][$year][$mid]['stocks'] = $unique_stocks;
      }
    }
    
    return $results;
  }
   
  /**
   * Given an array of BookingEvents the calendar is updated with regards to the
   * events that are relevant to the product this calendar refers to
   *
   * @param $events
   * An array of events to update the calendar with
   *
   * @return array - An array of response on whether event updates were succesful or not
   */
  public function updateCalendar(array $events) {
    
    $responses = array();
        
    $monthly_events = array();

    foreach ($events as $event) {
      // Make sure event refers to the product for this calendar
      if ($event->product_id == $this->product_id) {
        // If the event is in the same month span just queue to be added
        if ($event->sameMonth()) {
          $monthly_events[] = $event;
        }
        else {
          // Check if multi-year - if not just create monthly events
          if ($event->sameYear()) {
            $monthly_events_tmp = array();
            $monthly_events_tmp = $event->transformToMonthlyEvents();
            $monthly_events =  array_merge($monthly_events, $monthly_events_tmp);
          }
          else {
            // else transform to single years and then to monthly
            $yearly_events = $event->transformToYearlyEvents();
            foreach ($yearly_events as $ye) {
              $monthly_events_tmp = array();
              $monthly_events_tmp = $ye->transformToMonthlyEvents();
              $monthly_events =  array_merge($monthly_events, $monthly_events_tmp);
            }
          }
        }
      }
      else {
        $response[$event->id] = ROOMS_WRONG_UNIT;
      }
    }        

    foreach ($monthly_events as $event) {
      $this->addMonthEvent($event);
      $response[$event->id] = ROOMS_UPDATED;
    }
    
    return $response;
  }

  
  /**
   * Adds an event to the calendar
   *
   * @param $event
   *   An an event of type BookingEvent 
   *
   * @return
   *   TRUE if events added, FALSE if some event failed
   */
  public function addMonthEvent($event) {
    // First check if the month exists and do an update if so
    if ($this->monthDefined($event->startMonth(), $event->startYear())) {
      $partial_month_row = $this->preparePartialMonthArray($event);
      $update = db_update($this->table)
        ->condition('product_id', $this->product_id)
        ->condition('month', $event->startMonth())
        ->condition('year', $event->startYear())
        ->fields($partial_month_row)
        ->execute();
    }
    // Do an insert for a new month
    else {
      // Prepare the days array
      $days = $this->prepareFullMonthArray($event);
      $month_row = array(
        'product_id' => $this->product_id,
        'year' => $event->startYear(),
        'month' => $event->startMonth(),        
      );
      $month_row = array_merge($month_row, $days);
      
      $insert = db_insert($this->table)->fields($month_row);
      $result = $insert->execute();
    }
  }
    
  
  /**
   * Given an event it prepares the entire month array for it
   * assuming no other events in the month and days where there
   * is no event get set to the default stock;
   */
  protected function prepareFullMonthArray($event) {
    $days = array();
    $eod = rooms_end_of_month_dates($event->startYear());
    $last_day = $eod[$event->startMonth()];

    for ($i = 1; $i<=$last_day; $i++) {
      if (($i >= $event->startDay()) && ($i <= $event->endDay())) {
        $days['d' . $i] = $event->id;
      }
      else {
        $days['d' . $i] = $this->default_stock; //replace with default stock
      }
    }
    return $days;
  }
  
  
  /**
   * Given an event it prepares a partial array covering just the days
   * for which the event is involved
   */
  protected function preparePartialMonthArray($event) {
    $days = array();
    for ($i = $event->startDay(); $i<=$event->endDay(); $i++) {
        $days['d' . $i] = $event->id;
    }
    return $days;
  }
  
  /**
   * Returns the default stock
   */
  public function getDefaultStock() {
    return $this->default_stock;
  }
  
  /**
   * Check if a month exists
   *
   * @return true - if the month is defined
   */
  public function monthDefined($month, $year) {
  
    $query = db_select($this->table, 'a');
    $query->addField('a', 'product_id');
    $query->addField('a', 'year');
    $query->addField('a', 'month');
    $query->condition('a.product_id', $this->product_id);
    $query->condition('a.year', $year);
    $query->condition('a.month', $month);
    $result = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
    if (count($result) > 0) {
      return TRUE;
    } 
    return FALSE;
  }
  
  
}
