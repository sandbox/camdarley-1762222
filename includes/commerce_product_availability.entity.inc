<?php

function commerce_product_availability_entity_property_info() {

  $info = array();

  // Add meta-data about the basic commerce_line_item properties.
  $properties = &$info['commerce_line_item']['properties'];
  
  $properties['availability'] = array(
    'label' => t('Availability', array(), array('context' => 'a drupal commerce availability number')),
    'type' => 'decimal',
    'description' => t('The line item availability.'),
    'getter callback' => 'commerce_product_availability_get_properties',
    'setter callback' => 'commerce_product_availability_set_properties',
    'setter permission' => 'administer line items',
    'required' => FALSE,
    'clear' => array('availability_value'),
  );
  
  return $info;
}

/**
 * Callback for getting line item properties.
 *
 * @see commerce_line_item_entity_property_info()
 */
function commerce_product_availability_get_properties($line_item, array $options, $name) {
  switch ($name) {
    case 'availability':
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      $product_id = $line_item_wrapper->commerce_product->product_id->value();
      $product = commerce_product_load($product_id);
      $date = $line_item->commerce_booking_date[LANGUAGE_NONE][0]['value'];
      return !empty($line_item->availability) ? $line_item->availability : commerce_product_availability_on_date($product);
  }
}

/**
 * Callback for setting line item properties.
 *
 * @see commerce_line_item_entity_property_info()
 */
function commerce_product_availability_set_properties($line_item, $name, $value) {
  switch ($name) {
    case 'availability':
      $line_item->availability = $value;
      break;
  }
}