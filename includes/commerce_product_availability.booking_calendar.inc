<?php

/**
 * Handles quering and updating the availability information
 * relative to a single bookable product.
 */
class BookingCalendar {

  // The bookable product the Calendar is relevant to  
  protected $product_id;
  
  public function __construct($product_id) {
    $this->product_id = $product_id;
  }
  
  
  /**
   * Given a date range returns an array of BookingEvents. The heavy lifting really takes place in
   * the getRawDayData function - here we are simply acting as a factory for event objects
   *
   * @param $start_date
   * The starting date
   *
   * @param $end_date
   * The end date of our range
   *
   * @returns
   * An array of BookingEvent objects
   */
  public function getEvents(DateTime $start_date, DateTime $end_date) {
     
    // Get the raw day results
    $results = $this->getRawDayData($start_date, $end_date);
    $events = array();
    
    
    foreach ($results as $date => $quantity) {
      $date_object = new DateTime($date);
      
      $event = new ProductBookingEvent($this->product_id,
                                    $quantity,
                                    $date_object,
                                    $date_object,
                                    true);
		  
      
      $events[] = $event;
    }
    return $events;
  }
  
  /**
   * Given a date range it returns all data within that range including the start and
   * end dates of stocks. The MySQL queries are kept simple and then the data is cleared up
   * 
   * @param $start_date
   * The starting date
   *
   * @param $end_date
   * The end date of our range
   *
   * @return
   * An array of the structure data[productid][year][month][days][d1]..[d31]
   * as wekk as data[productid][year][month][unique_stocks]
   */
  public function getRawDayData(DateTime $start_date, DateTime $end_date) {
    
    
    
    $results = array();
    
	  $query = db_select('commerce_line_item', 'a');
	  $query->fields('a', array('quantity'));
	  $query->fields('d', array('commerce_booking_date_value'));
	  $query->fields('p', array('commerce_product_product_id'));
	  $query->leftJoin('commerce_order', 'o', 'a.order_id = o.order_id');
	  $query->leftJoin('field_data_commerce_customer_billing', 'b', 'o.order_id = b.entity_id AND b.entity_type = \'commercer_order\' AND b.deleted = 0');
	  $query->leftJoin('field_data_commerce_booking_date', 'd', 'a.line_item_id = d.entity_id AND d.entity_type = \'commerce_line_item\' AND d.deleted = 0');
	  $query->leftJoin('field_data_commerce_product', 'p', 'p.entity_id = a.line_item_id');
	  $query->where("DATE_FORMAT(d.commerce_booking_date_value, '%Y-%m-%d') >= :start_date AND DATE_FORMAT(d.commerce_booking_date_value, '%Y-%m-%d') <= :end_date", array(':start_date' => $start_date->format('Y-m-d'), ':end_date' => $end_date->format('Y-m-d')));
	  $query->where("DATE_FORMAT(d.commerce_booking_date_value, '%Y-%m-%d') <= :date", array(':date' => $end_date->format('Y-m-d')));
	  $query->condition('p.commerce_product_product_id', $this->product_id);
	  $query->condition('a.type', array('bookable_product'), 'IN');
	  $query->condition('o.status', array('pending', 'processing', 'completed'), 'IN');
	  $query_results = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
	  
	  if (count($query_results) > 0) {
	    foreach ($query_results as $query_result) {
	      $quantity = $query_result['quantity'];
	      $date = $query_result['commerce_booking_date_value'];
	      if (isset($results[$date])) $results[$date] += $quantity;
	      else $results[$date] = $quantity;
	    }
	  }
    
    return $results;
  }
  
}
