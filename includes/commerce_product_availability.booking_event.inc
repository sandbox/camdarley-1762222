<?php

class ProductBookingEvent {
  
  // The booking unit the event is relevant to
  public $product_id;
  
  // The start date for the event
  public $start_date;
  
  // The end date for the event
  public $end_date;
  
  // The type of event
  public $id;
  
  // The difference with available stock
  public $left_stock;
  
  
  public function __construct($product_id, $event_id, $start_date, $end_date, $booked = false) {
    $this->product_id = $product_id;
    $this->id = $event_id;
    $this->start_date = $start_date;
    $this->end_date = $end_date;
    $this->booked = $booked;
  }
  
  public function startDay($format = 'j') {
    return $this->start_date->format($format);
  }
  
  public function startMonth($format = 'n') {
    return $this->start_date->format($format);
  }
  
  public function startYear($format = 'Y') {
    return $this->start_date->format($format);
  }
  
  public function endDay($format = 'j') {
    return $this->end_date->format($format);
  }
  
  public function endMonth($format = 'n') {
    return $this->end_date->format($format);
  }
  
  public function endYear($format = 'Y') {
    return $this->end_date->format($format);
  }
  
  /**
   * Returns the months involved in the events
   */
  public function months() {
  }

  public function diff() {
    $interval = $this->start_date->diff($this->end_date);
    return $interval;
  }
  
  public function sameMonth() {
    if (($this->startMonth() == $this->endMonth()) && ($this->startYear() == $this->endYear())) {
      return TRUE;
    }
    return FALSE;
  }
  
  public function sameYear() {
    
    if ($this->startYear() == $this->endYear()) {
      return TRUE;
    }
    return FALSE;
  }  
  
  /**
   * Takes an event that spans several years and transforms it to
   * yearly events
   */
  public function transformToYearlyEvents() {
    // If same year return the event
    if ($this->sameYear()) {
      $sd = new DateTime();
      $sd->setDate($this->startYear(), $this->startMonth(), $this->startDay());
      $ed = new DateTime();
      $ed->setDate($this->endYear(), $this->endMonth(), $this->endDay());
      $be = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      return array($be);
    }
    
    // Else split into years
    $events = array();
    for ($i = $this->startYear(); $i <= $this->endYear(); $i++) {
      $sd = new DateTime();
      $ed = new DateTime();
      if ($i == $this->startYear()) {
        $sd->setDate($i, $this->startMonth(), $this->startDay());
        $ed->setDate($i, 12, 31);
        $events[$i] = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      }
      elseif ($i == $this->endYear()) {
        $sd->setDate($i, 1, 1);
        $ed->setDate($i, $this->endMonth(), $this->endDay());
        $events[$i] = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      }
      else {
        $sd->setDate($i, 1, 1);
        $ed->setDate($i, 12, 31);
        $events[$i] = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      }
    }
    
    return $events;
  }
  
  /**
   * Takes a single event that spans several months and transforms it to
   * monthly events - this assumes that the event is contained within a year
   */
  public function transformToMonthlyEvents() {
    $events = array();
    //First we need to split into events in separate years
    if (!$this->sameYear()) {
      return FALSE;
    }
    if ($this->sameMonth()) {
      $sd = new DateTime();
      $sd->setDate($this->startYear(), $this->startMonth(), $this->startDay());
      $ed = new DateTime();
      $ed->setDate($this->endYear(), $this->endMonth(), $this->endDay());
      $be = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      return array($be);
    }
    $months = rooms_end_of_month_dates($this->startYear());

    for ($i = $this->startMonth(); $i <= $this->endMonth(); $i++) {
      $sd = new DateTime();
      $ed = new DateTime();
      if ($i == $this->startMonth()) {
        $sd->setDate($this->startYear() , $i, $this->startDay());
        $ed->setDate($this->startYear(), $i, $months[$i]);
        $events[$i] = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      }
      elseif ($i == $this->endMonth()) {
        $sd->setDate($this->startYear(), $i, 1);
        $ed->setDate($this->startYear(), $i, $this->endDay());
        $events[$i] = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      }
      else{
        $sd->setDate($this->startYear(), $i, 1);
        $ed->setDate($this->startYear(), $i, $months[$i]);
        $events[$i] = new ProductBookingEvent($this->product_id, $this->id, $sd, $ed);
      }
    }
    return $events;
  }
  
  public function calculateAvailabiliy($availability_events) {
    foreach($availability_events as $event_key => $availability_event) {
       if (($this->start_date >= $availability_event->start_date) && ($this->end_date <= $availability_event->end_date)) {
         $left_stock_value = $availability_event->id - $this->id;
         $this->left_stock = $left_stock_value;
       }
     }
  }
  
    
  /**
   * Return event in a format amenable to FullCalendar display or generally
   * sensible json
   */
  public function formatJson($style = ROOMS_AVAILABILITY_ADMIN_STYLE) {
    $event = array(
      'id' => $this->id,
      'start' => $this->startYear() . '-' . $this->startMonth('m') . '-' . $this->startDay('d') . 'T13:00:00Z',
      'end' => $this->endYear() . '-' . $this->endMonth('m') . '-' . $this->endDay('d') . 'T13:00:00Z', 
      'title' => $this->id,
    );
    
    if ($style == ROOMS_AVAILABILITY_GENERIC_STYLE) {
      if ($this->booked) {
        if (isset($this->left_stock) && ($this->left_stock <= 0)) {
          $event['title'] = "N/A";
          $event['color'] = 'red';
        }
        else {
          $event['title'] = "OK";
          $event['color']  = 'green';
        }
      }
      else {
        if ($this->id == 0) {
          $event['title'] = "N/A";
          $event['color'] = 'red';
        }
        else {
          $event['title'] = "OK";
          $event['color']  = 'green';
        }
      }
    }
    else {
      if ($this->booked) {
        $event['className'] = "booked";
        $event['title'] = 'BKD: ' . $this->id;
        if (isset($this->left_stock) && ($this->left_stock <= 0)) {
          $event['color'] = 'red';
        }
        else $event['color']  = 'green';
      }
      else {
        if ($this->id == 0) {
          $event['title'] = "N/A";
          $event['color'] = 'red';
        }
      }
    }
    
    return $event;
  }
  
}